package dev.fredmosc.prepersistteste.controller;

import dev.fredmosc.prepersistteste.domain.User;
import dev.fredmosc.prepersistteste.dto.UserDTO;
import dev.fredmosc.prepersistteste.dto.UserForm;
import dev.fredmosc.prepersistteste.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    private static final String ID_NOT_FOUND = "Id not found";

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Autowired
    public UserController(ModelMapper modelMapper, UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
    }

    @GetMapping
    public ResponseEntity<List<User>> findAll() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @GetMapping("/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<UserDTO> findById(@PathVariable Long id) {
        var foundUser = userRepository.findById(id).orElseThrow(() -> {
            log.error(ID_NOT_FOUND);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ID_NOT_FOUND);
        });
        return ResponseEntity.ok(UserDTO.from(foundUser));
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody UserForm user) {
        return ResponseEntity.ok(UserDTO.from(userRepository.save(User.from(user))));
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<UserDTO> update(@PathVariable Long id, @RequestBody UserDTO user) {
        var foundUser = userRepository.findById(id).orElseThrow(() -> {
            log.error(ID_NOT_FOUND);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ID_NOT_FOUND);
        });

        modelMapper.map(user, foundUser);

        return ResponseEntity.ok(UserDTO.from(foundUser));

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id) {
        var foundUser = userRepository.findById(id).orElseThrow(() -> {
            log.error(ID_NOT_FOUND);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ID_NOT_FOUND);
        });
        userRepository.delete(foundUser);
    }
}
