package dev.fredmosc.prepersistteste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrepersistTesteApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrepersistTesteApplication.class, args);
    }

}
