package dev.fredmosc.prepersistteste.repository;

import dev.fredmosc.prepersistteste.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
